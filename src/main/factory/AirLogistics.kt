package main.factory

class AirLogistics : Logistics() {
    override fun createTransport(): Transport {
        return AirPlane()
    }
}