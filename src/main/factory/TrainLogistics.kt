package main.factory

class TrainLogistics : Logistics() {
    override fun createTransport(): Transport {
        return Train()
    }
}