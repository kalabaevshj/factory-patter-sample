package main.factory

class SeaLogistics : Logistics() {
    override fun createTransport(): Transport {
        return Ship()
    }
}