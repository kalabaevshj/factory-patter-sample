package main.factory

class RoadLogistics : Logistics() {
    override fun createTransport(): Transport {
        return Truck()
    }
}