package main.factory

import java.util.Scanner

class LogisticsApp {
    companion object {
        val nameOfLogistics = listOf(
            "Teńiz",
            "Quraqlıq",
            "Hawa",
            "Temir jol"
        )
        val logisticsCompanies = listOf(
            SeaLogistics(),
            RoadLogistics(),
            AirLogistics(),
            TrainLogistics()
        )
    }
    private lateinit var logistics: Logistics
    val sc = Scanner(System.`in`)
    fun run() {
        println("Tovarıńızdı ne arqalı jiberejaqsız?")
        for (i in nameOfLogistics.indices) {
            println("${i+1}. ${nameOfLogistics[i]} arqalı jiberiw")
        }
        select()
        logistics.delivery()
    }

    private fun select() {
        val select = sc.nextInt()
        try {
            logistics = logisticsCompanies[select-1]
        } catch (e: Exception) {
            println("Kiritiwde qátelik! Iltimas, jańadan kiritiń")
            select()
        }
    }
}