package main.factory

interface Transport {
    fun delivery()
}