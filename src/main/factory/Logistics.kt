package main.factory

abstract class Logistics {
    fun delivery() {
        createTransport().delivery()
    }

    abstract fun createTransport() : Transport
}